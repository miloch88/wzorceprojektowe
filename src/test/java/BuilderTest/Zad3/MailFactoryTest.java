package BuilderTest.Zad3;

import Builder.zad3.Mail;
import Builder.zad3.MailFactory;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

public class MailFactoryTest {

    private MailFactory mailFactory;

    @Before
    public void setup(){
        mailFactory = new MailFactory();
    }

    @Test
    public void isCorrectedMessage(){
    Assertions.assertThat(MailFactory.creatWarning("Yeti")).isEqualTo("Mayday Mayday Mayday Yeti");

    }
}
