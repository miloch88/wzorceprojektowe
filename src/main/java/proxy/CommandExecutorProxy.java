package proxy;

public class CommandExecutorProxy implements CommanfExecutor {

    private boolean isAdmin;
    private CommanfExecutor executor;

    public CommandExecutorProxy(String user, String pwd)  {
        if("Pankaj".equals(user) && "JAunralD$v".equals(pwd)) isAdmin = true;
        executor = new CommandExecutorImpl();
    }

    @Override
    public void runCommand(String cmd) throws Exception {
        if(isAdmin){
            executor.runCommand(cmd);
        }else{
            if(cmd.trim().startsWith("rm")){
                throw new Exception("rm command is not allowed for non-admin user");
            }else {
                executor.runCommand(cmd);
            }
        }
    }
}
