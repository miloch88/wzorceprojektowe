package proxy;

public interface CommanfExecutor {
    public void runCommand(String cmd) throws Exception;
}
