package proxy;

public class ProxyPatternTest {
    public static void main(String[] args) {
        CommanfExecutor executor = new CommandExecutorProxy("Pankaj", "wrong_pwd");

        try {
            executor.runCommand("ls -ltr");
            executor.runCommand(" rm -rf abc.pdf");
        } catch (Exception e){
            System.out.printf("Exceptin Message::" + e.getMessage());
        }
    }
}
