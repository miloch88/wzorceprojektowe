package Builder.zad2;

public class Main {
    public static void main(String[] args) {

        Stamp.Builder stampBegining = new Stamp.Builder()
                .setFirstDayNumber(0)
                .setSecondDayNumber(1)
                .setFirstMonthNumber(0)
                .setSecondMonthNumber(1)
                .setYearNumber1(2)
                .setYearNumber2(0)
                .setYearNumber3(0)
                .setYearNumber4(1)
                .setCaseNumber(1);


        Stamp stamp1 = stampBegining.createStamp();
        System.out.println(stamp1);

        Stamp stamp2 = stampBegining
                .setSecondDayNumber(3)
                .setSecondMonthNumber(8)
                .setYearNumber3(1)
                .setYearNumber4(8)
                .setCaseNumber(2)
                .createStamp();

        System.out.println(stamp2);

    }
}
