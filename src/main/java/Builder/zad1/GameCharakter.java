package Builder.zad1;

public class GameCharakter {

    //I. Stworzyć pola
    String name;
    int health, mana, numberOfPoints;

    //II. Stworzyć pełen konstruktor
    public GameCharakter(String name, int health, int mana, int numberOfPoints) {
        this.name = name;
        this.health = health;
        this.mana = mana;
        this.numberOfPoints = numberOfPoints;
    }
    //III. Stworzyć public static class Builder
    //Na konstruktorze refractor/ Replace c
       public static class Builder{

        private String name;
        private int health;
        private int mana;
        private int numberOfPoints;

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setHealth(int health) {
            this.health = health;
            return this;
        }

        public Builder setMana(int mana) {
            this.mana = mana;
            return this;
        }

        public Builder setNumberOfPoints(int numberOfPoints) {
            this.numberOfPoints = numberOfPoints;
            return this;
        }

        public GameCharakter createGameCharakter() {
            return new GameCharakter(name, health, mana, numberOfPoints);
        }
    }

    // alt + insert
    @Override
    public String toString() {
        return "GameCharakter{" +
                "name='" + name + '\'' +
                ", health=" + health +
                ", mana=" + mana +
                ", numberOfPoints=" + numberOfPoints +
                '}';
    }

//    @Override
//    public String toString() {
//        return String.format("Name: %s, helath: %d, mana: %d, points: %d \n" , name, health, mana, numberOfPoints);
//    }
}
