package Builder.zad1;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<GameCharakter> listOfGameCharakters = new ArrayList<GameCharakter>();

        listOfGameCharakters.add(new GameCharakter.Builder()
                .setName("Zdzichu")
                .setHealth(100)
                .setMana(50)
                .setNumberOfPoints(15)
                .createGameCharakter());


        GameCharakter.Builder hero1 = new GameCharakter.Builder()
                .setName("Zdzichu")
                .setHealth(100)
                .setMana(50)
                .setNumberOfPoints(15);
//                .createGameCharakter();

        GameCharakter hero2 = hero1.createGameCharakter();
        hero1.setName("Zdzichaaaaaaa");

        GameCharakter hero3 = hero1.createGameCharakter();

        listOfGameCharakters.add(new GameCharakter.Builder()
                .setName("Duży Zdzichu")
                .setHealth(50)
                .setMana(10)
                .setNumberOfPoints(10)
                .createGameCharakter());

        listOfGameCharakters.add(new GameCharakter.Builder()
                .setName("Mała Zdzicha")
                .setHealth(10)
                .setMana(5)
                .setNumberOfPoints(5)
                .createGameCharakter());

        System.out.println(listOfGameCharakters);


    }
}
