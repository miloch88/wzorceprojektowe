package Builder.zad3;

import java.util.Date;

public class Mail {

    private String tresc, nadawca, nazwaSerweraPosredniego, nazwaSkrzynkiPocztowej;
    private Date dataNadania, getDataOdbioru;
    private Type type;
    private boolean szyfrowane, spam;

    public Mail(String tresc, String nadawca, String nazwaSerweraPosredniego, String nazwaSkrzynkiPocztowej, Date dataNadania, Date getDataOdbioru, Type type, boolean szyfrowane, boolean spam) {
        this.tresc = tresc;
        this.nadawca = nadawca;
        this.nazwaSerweraPosredniego = nazwaSerweraPosredniego;
        this.nazwaSkrzynkiPocztowej = nazwaSkrzynkiPocztowej;
        this.dataNadania = dataNadania;
        this.getDataOdbioru = getDataOdbioru;
        this.type = type;
        this.szyfrowane = szyfrowane;
        this.spam = spam;
    }

    public static class Buldier{

        private String tresc;
        private String nadawca;
        private String nazwaSerweraPosredniego;
        private String nazwaSkrzynkiPocztowej;
        private Date dataNadania;
        private Date getDataOdbioru;
        private Type type;
        private boolean szyfrowane;
        private boolean spam;

        public Buldier setTresc(String tresc) {
            this.tresc = tresc;
            return this;
        }

        public Buldier setNadawca(String nadawca) {
            this.nadawca = nadawca;
            return this;
        }

        public Buldier setNazwaSerweraPosredniego(String nazwaSerweraPosredniego) {
            this.nazwaSerweraPosredniego = nazwaSerweraPosredniego;
            return this;
        }

        public Buldier setNazwaSkrzynkiPocztowej(String nazwaSkrzynkiPocztowej) {
            this.nazwaSkrzynkiPocztowej = nazwaSkrzynkiPocztowej;
            return this;
        }

        public Buldier setDataNadania(Date dataNadania) {
            this.dataNadania = dataNadania;
            return this;
        }

        public Buldier setGetDataOdbioru(Date getDataOdbioru) {
            this.getDataOdbioru = getDataOdbioru;
            return this;
        }

        public Buldier setType(Type type) {
            this.type = type;
            return this;
        }

        public Buldier setSzyfrowane(boolean szyfrowane) {
            this.szyfrowane = szyfrowane;
            return this;
        }

        public Buldier setSpam(boolean spam) {
            this.spam = spam;
            return this;
        }

        public Mail createMail() {
            return new Mail(tresc, nadawca, nazwaSerweraPosredniego, nazwaSkrzynkiPocztowej, dataNadania, getDataOdbioru, type, szyfrowane, spam);
        }
    }

    public String getTresc() {
        return tresc;
    }

    public void setTresc(String tresc) {
        this.tresc = tresc;
    }

    public String getNadawca() {
        return nadawca;
    }

    public void setNadawca(String nadawca) {
        this.nadawca = nadawca;
    }

    public String getNazwaSerweraPosredniego() {
        return nazwaSerweraPosredniego;
    }

    public void setNazwaSerweraPosredniego(String nazwaSerweraPosredniego) {
        this.nazwaSerweraPosredniego = nazwaSerweraPosredniego;
    }

    public String getNazwaSkrzynkiPocztowej() {
        return nazwaSkrzynkiPocztowej;
    }

    public void setNazwaSkrzynkiPocztowej(String nazwaSkrzynkiPocztowej) {
        this.nazwaSkrzynkiPocztowej = nazwaSkrzynkiPocztowej;
    }

    public Date getDataNadania() {
        return dataNadania;
    }

    public void setDataNadania(Date dataNadania) {
        this.dataNadania = dataNadania;
    }

    public Date getGetDataOdbioru() {
        return getDataOdbioru;
    }

    public void setGetDataOdbioru(Date getDataOdbioru) {
        this.getDataOdbioru = getDataOdbioru;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public boolean isSzyfrowane() {
        return szyfrowane;
    }

    public void setSzyfrowane(boolean szyfrowane) {
        this.szyfrowane = szyfrowane;
    }

    public boolean isSpam() {
        return spam;
    }

    public void setSpam(boolean spam) {
        this.spam = spam;
    }

    @Override
    public String toString() {
        return String.format("\""+tresc+"\"");
    }
}
