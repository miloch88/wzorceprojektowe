package Builder.zad3;

import java.util.ArrayList;
import java.util.List;

public class Client {

    private String name;
    private List<Mail> list = new ArrayList<>();

    public Client(String name) {
        this.name = name;
    }

    public void readMail(Mail mail){
        list.add(mail);
        System.out.printf("Klient %s otrzymał maila o treści %s\n", this.getName(), mail.getTresc());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Mail> getList() {
        return list;
    }

    public void setList(List<Mail> list) {
        this.list = list;
    }
}
