package Builder.zad3;

import java.util.ArrayList;
import java.util.List;

public class MailServer {

    private List<Client> clients = new ArrayList<Client>();

    public void connect(Client client) {
        clients.add(client);
    }

    public void disconnect(Client client) {
        if (clients.contains(client)) {
            clients.remove(client);
        }
    }

    public void sendMessage(Mail mail, Client sender) {
        for (Client client : clients){
            if(!client.equals(sender)){
                client.readMail(mail);
            }
        }
    }
}
