package Builder.zad3;

import java.util.Date;

public class MailFactory {

    public static Mail createNotification(String oferta){
        return new Mail.Buldier().setSzyfrowane(false)
                .setType(Type.OFFER)
                .setDataNadania(new Date())
                .setNadawca("abc@abc.pl")
                .setSpam(false)
                .setTresc("ABCDEFGHIJK")
                .createMail();
    }

    public static Mail creatWarning(String warining){
        return new Mail.Buldier().setSzyfrowane(true)
                .setType(Type.NOTIFICATIONS)
                .setDataNadania(new Date())
                .setNadawca("mayday@mayday.com")
                .setSpam(false)
                .setTresc("Mayday Mayday Mayday " + warining)
                .createMail();
    }
}
