package Builder.zad3;

public class Main {
    public static void main(String[] args) {

        MailServer mailServer = new MailServer();
        Client michal = new Client("Michał");

        mailServer.connect(new Client("Marian"));
        mailServer.connect(michal);
        mailServer.connect(new Client("Piotrek"));
        mailServer.connect(new Client("Władek"));

        mailServer.sendMessage(MailFactory.creatWarning("Yeti"),michal);
    }
}
