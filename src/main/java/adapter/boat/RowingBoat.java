package adapter.boat;

public interface RowingBoat {
    void row();
}
