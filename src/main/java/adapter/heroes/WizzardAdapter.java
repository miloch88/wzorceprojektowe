package adapter.heroes;

public class WizzardAdapter implements Fighter {

    private final Wizzard wizzard;

    public WizzardAdapter() {
        this.wizzard = new Wizzard();
    }

    @Override
    public void attack() {
        this.wizzard.castDestructionSpell();
    }

    @Override
    public void defend() {
        this.wizzard.shield();
    }

    @Override
    public void escape() {
        this.wizzard.portal();
    }
}
