package adapter.heroes;

public class WarriorAdapter implements Fighter {

    private final Warrior warrior;

    public WarriorAdapter(){
        this.warrior = new Warrior();
    }

    @Override
    public void attack() {
        this.warrior.useSword();
        System.out.println("Wojownik po ataku musi chwilę odpocząć");
    }

    @Override
    public void defend() {
        this.warrior.RiseShield();
        System.out.println("Obronił króla i dostał dodatkowy żołd");
        this.warrior.goTheChicks();
    }

    @Override
    public void escape() {
        this.warrior.layDrunk();
    }
}
