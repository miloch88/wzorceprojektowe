package adapter.heroes;

public class Warrior {
    public void useSword(){
        System.out.println("Wojownik zaatakował mieczem");
    }

    public void RiseShield(){
        System.out.println("Wojownik podnosi tarczę i kryje się za nią");
    }

    public void goTheChicks(){
        System.out.println("Wojownik idzie do baru i podrywa księżniczki");
    }

    public void layDrunk(){
        System.out.println("Upija się i udaje martwego");
    }

    public void pickUpArtefact(){
        System.out.println("Przechulał całą kasę, ale znalazł pierścień");
    }
}
