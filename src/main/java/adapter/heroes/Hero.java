package adapter.heroes;


//KORZYSTAMY Z ZASADY LISKOV

import java.util.Arrays;
import java.util.Iterator;

public class Hero {
    private Fighter[] army;

    public Hero(int sizeOfArmy){
        army = new Fighter[sizeOfArmy];
        for (int i = 0; i < sizeOfArmy; i++) {
            if(i < sizeOfArmy/2 ){
                army[i] = new WizzardAdapter();
            }else {
                army[i] = new WarriorAdapter();
            }
        }
    }

    public void Charge(){
        for (Fighter f : army) {
            f.attack();
        }
    }

    public void allMagesCast(){
        for(Fighter f: army){
            if(f instanceof WarriorAdapter){
                f.attack();
            }
        }
    }

    public void sendOneUnit(){
        Iterator iterator = Arrays.asList(army).iterator();
        if(iterator.hasNext()){
            Fighter unit = (Fighter) iterator.next();
            unit.attack();
        }
    }
}
