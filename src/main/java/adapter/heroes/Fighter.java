package adapter.heroes;

public interface Fighter {
    void attack();
    void defend();
    void escape();
}
