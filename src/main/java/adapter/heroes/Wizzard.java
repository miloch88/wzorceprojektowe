package adapter.heroes;

public class Wizzard {
    public void castDestructionSpell(){
        System.out.println("Czarownik rzuca czar i niszczy wszystko");
    }

    public void shield(){
        System.out.println("Czarownik rzuca czar ochronny");
    }

    public void portal(){
        System.out.println("Czarownik otwiera portal i spierdala");
    }
}
