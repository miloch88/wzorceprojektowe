package abstractFactory.zad1;

public class AsusPC extends AbstractPC {

    private AsusPC(String name, int cpu_power, double gpu_power, boolean isOverlocked) {
        super(name, COMPUTER_BRAND.ASUS, cpu_power, gpu_power, isOverlocked);

        System.out.println("Stworzono: " + this.getName());
    }

    public static AbstractPC createAsusPC01(){
        return new AsusPC("Asus 01", 100, 90, true);
    }

    public static AbstractPC createAsusPC02(){
        return new AsusPC("Asus 02", 200, 45, false);
    }

}
