/*
Fabryka Abstrakcyjna: konstrukcyjny wzorzec projektowy. Co daje?
- Przy odpowiednim użyciu (package protected) klient oddzielony jest od implementacji,
- Możemy ukryć klasę przed użytkownikiem (package protected)

- Posiadamy jedną klasę która pozwala nam na tworzenie różnych obiektów
- Nie musimy przekazywać parametrów (ale możemy). Możemy przygotować konstruktor konkretnego obiektu.
W przykładzie createBMW16 nie przyjmuje parametrów, ale zwraca obiekt wywołania:
(Czyli spreparowany obiekt)

- Możemy ograniczyć użytkownikowi konieczność wpisywania części typów obiektów, dając mu pewnego rodzaju szablony obiektów
- Tworzymy klasy abstrakcyjne i w nich metody statyczne lub tworzymy klasę factory i do tworzenia obiektów tworzymy instancję tej klasy.
W powyższym przykładzie mamy FabrykęAbstrakcyjną (oddzielną klasę produkującą obiekty)

Jak tworzymy?
Stwórz model (klasę której obiekty będą wytwarzane przez fabrykę abstrakcyjną.
Stwórz klasę która jest abstrakcyjna
zaimplementuj w tej klasie metody (stworzeniowe) każda z nich powinna
zwracać pewien gotowy obiekt (w przykładzie używamy:
		return new Car(“bmw”, 30.0, 200, 4);
	czyli jakby:
return new DowolnyObiekt(parametr, parametr, parametr);
być statyczna
Korzystaj z klasy statycznie (bez słówka new lub obiektu):
zakładając że klasa abstrakcyjna nazywa się AbstractFactory, wywołanie wyglądałoby:
		DowolnyObiekt stworzony = AbstractFactory.metodaStworzeniowa();

Przykład:
https://bitbucket.org/nordeagda2/designpatternfactoryexample

 */

package abstractFactory.przyklad;

//Kiedy Car jest public w innym package
//import abstractFactory.przyklad.car.Car;
//CarFactory jest klasą abstract

//Podpunk II.
//import abstractFactory.przyklad.car.Car;
import abstractFactory.przyklad.car.CarFactory;
import abstractFactory.przyklad.car.ICar;

public class Main {
    public static void main(String[] args) {

//        Kiedy Car jest publiczny w innym package
//        Car car = new Car("Ford", 45.5, 240, 5);

//        CarFactory jest klasą abstract
//        CarFactory carFactory = new CarFactory();

//        Car jest public i możemy stworzyć szablon obiektów .createBMW16, ale możemy też
//                stworzyć nową instancję ręcznie
//        Car car = CarFactory.createBMW16();

        ICar iCar = CarFactory.createBMW16();
        System.out.println(iCar);

    }
}
