package abstractFactory.przyklad.car;

public interface ICar {

    void drive();
    int getSpeed();
    double getGasLevel();
    double getHorsepower();

}
