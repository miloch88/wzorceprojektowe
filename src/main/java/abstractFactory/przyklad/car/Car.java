/*
I. Tworzenia klasy Car (modelu) w osobnym package, ale class nie jest public więc nie
możemy stworzyć instancji w Mainie;
II. Stworzenie klasy abstrakcyjnej CarFaktory; Używamy tego do stwprzenia modelu
III. Stworzymy interface ICar, aby nie można było utworzyć więcej obiektów Car, a jedynie
    ICar;
 */

package abstractFactory.przyklad.car;


class Car implements ICar{

    private String manufacturer;
    private double gasLevel;
    private int topSpeed;
    private double horsepower;
    private int seats;

    public Car(String manufacturer, double gasLevel, int topSpeed, int seats) {
        this.manufacturer = manufacturer;
        this.gasLevel = gasLevel;
        this.topSpeed = topSpeed;
        this.seats = seats;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    @Override
    public void drive() {
        System.out.println("Jade samochodem marki: " + manufacturer);
    }

    @Override
    public int getSpeed() {
        return topSpeed;
    }

    public double getGasLevel() {
        return gasLevel;
    }

    public void setGasLevel(double gasLevel) {
        this.gasLevel = gasLevel;
    }

    public int getTopSpeed() {
        return topSpeed;
    }

    public void setTopSpeed(int topSpeed) {
        this.topSpeed = topSpeed;
    }

    public double getHorsepower() {
        return horsepower;
    }

    public void setHorsepower(double horsepower) {
        this.horsepower = horsepower;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    @Override
    public String toString() {
        return "Car{" +
                "manufacturer='" + manufacturer + '\'' +
                ", gasLevel=" + gasLevel +
                ", topSpeed=" + topSpeed +
                ", horsepower=" + horsepower +
                ", seats=" + seats +
                '}';
    }
}
