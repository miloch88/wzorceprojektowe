package abstractFactory.przyklad.car;

public abstract class CarFactory {

    //metoda musi być statyczna
    public static Car createBMW16(){
        return new Car("BMW", 30.0, 200, 4);
    }
}
