package abstractFactory.zad3;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Pizza {

    private String nazwa;
    private List<String> ingredients = new ArrayList<>();

    public Pizza(String nazwa) {
        this.nazwa = nazwa;
    }

    public Pizza(String nazwa, List<String> ingredients) {
        this.nazwa = nazwa;
        this.ingredients = ingredients;
    }

    public static class PizzaBuilder{

        private String nazwa;
        private List<String> ingredients;

        public PizzaBuilder setNazwa(String nazwa) {
            this.nazwa = nazwa;
            return this;
        }

        public PizzaBuilder setIngredients(List<String> ingredients) {
            this.ingredients = ingredients;
            return this;
        }

        public Pizza createPizza() {
            return new Pizza(nazwa);
        }
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public List<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pizza pizza = (Pizza) o;
        return Objects.equals(getNazwa(), pizza.getNazwa()) &&
                Objects.equals(getIngredients(), pizza.getIngredients())
                && getIngredients().containsAll(pizza.getIngredients())
                && pizza.getIngredients().containsAll(getIngredients())
                && getIngredients().size() == pizza.getIngredients().size();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNazwa(), getIngredients());
    }
}
