package abstractFactory.zad3;

public abstract class Pizzeria {

    public static Pizza stworzMargarrite(){
        Pizza pizza = new Pizza.PizzaBuilder().setNazwa("Margarritta").createPizza();

        pizza.getIngredients().add("Ser");
        pizza.getIngredients().add("Sos");
        pizza.getIngredients().add("Bazylia");

        return pizza;
    }

    public static Pizza stworzHawajska() {
        Pizza pizza = new Pizza("Hawajska");

        pizza.getIngredients().add("Ser");
        pizza.getIngredients().add("Sos");
        pizza.getIngredients().add("Bazylia");
        pizza.getIngredients().add("Ananas");
        pizza.getIngredients().add("Szynka");

        return pizza;
    }

    public static Pizza stworzWiejska() {
        Pizza pizza = new Pizza("Wiejska");

        pizza.getIngredients().add("Ser");
        pizza.getIngredients().add("Sos");
        pizza.getIngredients().add("Bazylia");
        pizza.getIngredients().add("Kiełbasa");
        pizza.getIngredients().add("Cebula");
        pizza.getIngredients().add("Boczek");

        return pizza;
    }




}
