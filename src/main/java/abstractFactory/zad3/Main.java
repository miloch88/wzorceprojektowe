/*
Zadanie 3:
Piszemy aplikację dla pizzerii. Stwórz klasę Pizza z listą składników i builderem, który posiada metodę dodania składnika. Po użyciu build stwórz pizzę.

Stwórz fabrykę pizz i 3 najbardziej popularne pizze. Użyj w fabryce metod stworzeniowych i wewnątrz metod twórz obiekty posługując się builderem.

Stwórz maina i w nim parser. Przetestuj aplikację komendami:

zamów HAWAJSKA
zamów SEROWA
zamów WIEJSKA

itd....

* Dopisz 3 rodzaje pizzy

 */

package abstractFactory.zad3;

import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Set<Pizza> pizzas = new HashSet<>();


        pizzas.add(Pizzeria.stworzHawajska());
//        pizzas.add(Pizzeria.stworzHawajska());
//        pizzas.add(Pizzeria.stworzHawajska());
        pizzas.add(Pizzeria.stworzMargarrite());
        pizzas.add(Pizzeria.stworzWiejska());

        System.out.println(pizzas);
        System.out.println(pizzas.size());

        System.out.println(Pizzeria.stworzHawajska().getIngredients());
    }
}
