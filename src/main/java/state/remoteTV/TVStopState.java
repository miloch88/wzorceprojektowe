package state.remoteTV;

public class TVStopState implements State {

    @Override
    public void doAction() {
        System.out.println("TV is turned OFF");
    }

    public void doSomthing(){
        System.out.println("Telewizor jest wyłączony");
    }

}
