package state.remoteTV;

public class Main {
    public static void main(String[] args) {

        System.out.println("Zadanie bez wzorca projektowego State: ");
        TVRemoteBasic remote = new TVRemoteBasic();

        remote.setState("on");
        remote.doAction();

        remote.setState("off");
        remote.doAction();
        System.out.println();

        System.out.println("Zadanie ze wzorcem projektowym State: ");
        TVContext context = new TVContext();

        context.setTvState(new TVStartState());
        context.doAction();
        context.doSomthing();

        context.setTvState(new TVStopState());
        context.doAction();
        context.doSomthing();
    }
}
