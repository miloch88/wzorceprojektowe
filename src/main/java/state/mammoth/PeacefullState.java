package state.mammoth;

public class PeacefullState implements State {

    private Mammoth mammoth;

    public PeacefullState(Mammoth mammoth) {
        this.mammoth = mammoth;
    }

    @Override
    public void onEnterState() {
        System.out.println("Mamoth calms down");
    }

    @Override
    public void observe() {
        System.out.println("Mammoth is calm and peacefull");
    }
}
