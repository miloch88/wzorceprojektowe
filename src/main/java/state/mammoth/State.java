package state.mammoth;

public interface State {

    void onEnterState();

    void observe();
}
