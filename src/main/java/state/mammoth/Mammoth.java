package state.mammoth;

public class Mammoth {

    private State state;

    public Mammoth() {
        state = new PeacefullState(this);
    }

    public void timePasses(){
        if(state.getClass().equals(PeacefullState.class)){
            changeStateTo(new AngryState(this));
        }else{
            changeStateTo(new PeacefullState(this));
        }
    }

    private void changeStateTo(State state) {
        this.state = state;
        this.state.onEnterState();
    }

    @Override
    public String toString() {
        return "Mammoth";
    }

    public void observe(){
        this.state.observe();
    }
}
