package stateStrategy;

public class ToUpperFormatter implements IFormatter {

    @Override
    public String format(String text) {
        return text.toUpperCase();
    }

}
