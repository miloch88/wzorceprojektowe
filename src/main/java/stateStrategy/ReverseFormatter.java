package stateStrategy;

public class ReverseFormatter implements IFormatter {
    @Override
    public String format(String text) {

        String text2 = new StringBuilder(text).reverse().toString();

        return text2;
    }
}
