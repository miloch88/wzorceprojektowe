package stateStrategy;

public class CapitalFormater implements IFormatter{
    @Override
    public String format(String text) {
        String[] table = text.trim().split(" ");

//        Tworzy kopię i nie zapisuje nigdzie drugiego s2, możemy dodać do Collection
//        for (String s : table) {
//            s2 = s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
//        }

        for (int i = 0; i < table.length; i++) {
            table[i] = table[i].substring(0, 1).toUpperCase() + table[i].substring(1).toLowerCase();
        }

        return String.join(" ", table );
    }
}
