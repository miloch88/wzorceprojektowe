package stateStrategy;

import javax.sound.midi.Soundbank;

public class Main {
    public static void main(String[] args) {

        String prezydent = "            Przydent przestrzega konstytucję    ";

        //zamień FormatyingType na liczbę
        System.out.println(StringFormat(FormattingType.TOLOWER, prezydent));

    }


    private static String StringFormat(FormattingType ft, String text) {
        //alt+ enter dla szybkiego wypisania enumów
        switch (ft) {

            case TOUPPER:
                return new ToUpperFormatter().format(text);
            case TOLOWER:
                return new ToLowerFormatter().format(text);
            case TRIM:
                return new TrimFormater().format(text);
            case CLEAN:
                return new CleanFormater().format(text);
            case CAPITAL:
                return new CapitalFormater().format(text);
            case ASCII:
                return new ToASCIIFormater().format(text);
            case REVERSE:
                return new ReverseFormatter().format(text);
            default:
                return "";

        }

//        IFormatter formatter = new ToUpperFormatter();
//        System.out.println(formatter.format(text));
//
//        formatter = new ToLowerFormatter();
//        System.out.println(formatter.format(text));
//
//        formatter = new TrimFormater();
//        System.out.println(formatter.format(text));
//
//        formatter = new CleanFormater();
//        System.out.println(formatter.format(text));
//
//        formatter = new CapitalFormater();
//        System.out.println(formatter.format(text));
//
//        formatter = new ReverseFormatter();
//        System.out.println(formatter.format(text));
//
//        formatter = new ToASCIIFormater();
//        System.out.println(formatter.format(text));
    }
}
