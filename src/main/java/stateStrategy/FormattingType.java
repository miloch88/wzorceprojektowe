package stateStrategy;

public enum FormattingType {

    TOUPPER,
    TOLOWER,
    TRIM,
    CLEAN,
    CAPITAL,
    ASCII,
    REVERSE


}

