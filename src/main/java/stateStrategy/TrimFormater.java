package stateStrategy;

public class TrimFormater implements IFormatter {
    @Override
    public String format(String text) {
        return text.trim();
    }
}
