package stateStrategy;

public interface IFormatter {
    String format(String text);
}
