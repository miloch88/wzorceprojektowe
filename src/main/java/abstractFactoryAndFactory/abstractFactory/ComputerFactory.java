package abstractFactoryAndFactory.abstractFactory;

import abstractFactoryAndFactory.Computer;

public class ComputerFactory {

    public static Computer getComputer(ComputerAbstractFactory factory){
        return factory.createComputer();
    }

}
