package abstractFactoryAndFactory.abstractFactory;

import abstractFactoryAndFactory.Computer;

public interface ComputerAbstractFactory {
    public Computer createComputer();
}
