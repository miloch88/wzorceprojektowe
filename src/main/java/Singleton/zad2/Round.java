package Singleton.zad2;

import java.util.Random;
import java.util.Scanner;

public class Round {

    private int number1, number2;
    private String action;
    private int correctedAnswer = number1*number2;

    public Round() {
        Random random = new Random();
        this.number1 = random.nextInt(MySettings.getMySettings().getZakresLiczby1());
        this.number2 = random.nextInt(MySettings.getMySettings().getZakresLiczby2());
        this.action = MySettings.getMySettings().getDzialanie();
    }

    public void showRiddle(){
        System.out.printf("Ile wynosi: %d %s %d: ", number1, action, number2);
    }

    public boolean readAnswer(){
        Scanner scanner = new Scanner(System.in);
        int answer = scanner.nextInt();

        switch (action){
            case ("+"):
                correctedAnswer = number1+number2;
                break;
            case ("-"):
                correctedAnswer = number1-number2;
                break;
            case ("*"):
                correctedAnswer = number1*number2;
                break;
            case ("/"):
                correctedAnswer = number1/number2;
        }

        if(correctedAnswer == answer){
            return true;
        }else{
            return false;
        }

    }

    public int getNumber1() {
        return number1;
    }

    public void setNumber1(int number1) {
        this.number1 = number1;
    }

    public int getNumber2() {
        return number2;
    }

    public void setNumber2(int number2) {
        this.number2 = number2;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
