package Singleton.zad2;

public class Game {

    private int roundNo = MySettings.getMySettings().getIloscRund();
    private int score = 0;

    public void carriedOutGame() {


        for (int i = 0; i < roundNo; i++) {
            Round round = new Round();

            round.showRiddle();
            if (round.readAnswer()) {
                score++;
                System.out.println("Prawdiłowa odpowiedź");
            } else {
                System.out.println("Zła odpowiedź");
            }

        }
        System.out.printf("Twój wynik to: %d", score);
    }

    public Game() {

    }
}

