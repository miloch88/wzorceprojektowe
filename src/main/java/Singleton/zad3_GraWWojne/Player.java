package Singleton.zad3_GraWWojne;

import java.util.ArrayList;
import java.util.List;

public class Player {

    private List<Karta> deckPlayer ;



    public Player() {
        this.deckPlayer =  new ArrayList<>();
    }

    @Override
    public String toString() {
        return "Player{" +
                "deckPlayer=" + deckPlayer +
                '}';
    }

    public List<Karta> getDeckPlayer() {
        return deckPlayer;
    }

    public void setDeckPlayer(List<Karta> deckPlayer) {
        this.deckPlayer = deckPlayer;
    }
}
