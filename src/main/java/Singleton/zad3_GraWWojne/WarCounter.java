package Singleton.zad3_GraWWojne;

public enum WarCounter {

    WAR_COUNTER;

    int countOfWar = 0;
    int winOfA = 0;
    int winOfB = 0;
    int tempBiggestWar = 0;
    int theBiggestWar = 0;

    public int getTempBiggestWar() {
        return tempBiggestWar;
    }

    public void setTempBiggestWar(int tempBiggestWar) {
        this.tempBiggestWar = tempBiggestWar;
    }



    public int getCountOfWar() {
        return countOfWar;
    }

    public void setCountOfWar(int countOfWar) {
        this.countOfWar = countOfWar;
    }

    public int getWinOfA() {
        return winOfA;
    }

    public void setWinOfA(int winOfA) {
        this.winOfA = winOfA;
    }

    public int getWinOfB() {
        return winOfB;
    }

    public void setWinOfB(int winOfB) {
        this.winOfB = winOfB;
    }

    public int getTheBiggestWar() {
        return theBiggestWar;
    }

    public void setTheBiggestWar(int theBiggestWar) {
        if(theBiggestWar > this.theBiggestWar){
        this.theBiggestWar = theBiggestWar;
        }
    }
}
