package Singleton.zad3_GraWWojne;

public enum Color {

    KIER("♥"), KARO("♦"), PIK("♠"), TREFL("♣");

    String symbol;

    Color(String symbol){
        this.symbol = symbol;
    }

    public String getSymbol(){
        return symbol;
    }
}
