package Singleton.zad3_GraWWojne;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Game {

    Player playerA;
    Player playerB;
    Deck deck;
    Random random = new Random();
    int noRound = 200;
    List<Karta> zdobyczeWojenne = new ArrayList<>();
    int warCount = 0;
    int iteration = 0;

    public Game(Player playerA, Player playerB, Deck deck) {
        this.playerA = playerA;
        this.playerB = playerB;
        this.deck = deck;
    }

    public void dealCards() {
//        while (deck.deck.size() != 0) {
//            int i = random.nextInt(deck.deck.size());
//            if (playerA.getDeckPlayer().size() == playerB.getDeckPlayer().size()) {
//                playerA.getDeckPlayer().add(deck.deck.get(i));
//                deck.deck.remove(i);
//            } else {
//                playerB.getDeckPlayer().add(deck.deck.get(i));
//                deck.deck.remove(i);
//            }
//        }

        for (int i = 0; i < deck.deck.size(); i++) {
            if (playerA.getDeckPlayer().size() == playerB.getDeckPlayer().size()) {
                playerA.getDeckPlayer().add(deck.deck.get(i));
                deck.deck.remove(i);
            } else {
                playerB.getDeckPlayer().add(deck.deck.get(i));
                deck.deck.remove(i);
            }
        }
    }

    public void war(int a, int b) {
        if (playerA.getDeckPlayer().size() == 0 && playerB.getDeckPlayer().size() == 0) {
            System.out.println("Polegli na polu chwały");
        } else if (playerA.getDeckPlayer().size() == 0) {
            playerB.getDeckPlayer().addAll(zdobyczeWojenne);
            zdobyczeWojenne.clear();
        } else if (playerB.getDeckPlayer().size() == 0) {
            playerA.getDeckPlayer().addAll(zdobyczeWojenne);
            zdobyczeWojenne.clear();
        } else {

            if (playerA.getDeckPlayer().get(a).figure.wartosc == playerB.getDeckPlayer().get(a).figure.wartosc) {
                zdobyczeWojenne.add(playerA.getDeckPlayer().get(a));
                zdobyczeWojenne.add(playerA.getDeckPlayer().get(b));
                playerA.getDeckPlayer().remove(a);
                playerB.getDeckPlayer().remove(b);
                war(a, b);
                WarCounter.WAR_COUNTER.setCountOfWar(WarCounter.WAR_COUNTER.getCountOfWar()+1);
                WarCounter.WAR_COUNTER.setTempBiggestWar(WarCounter.WAR_COUNTER.getTheBiggestWar()+1);


            } else if (playerA.getDeckPlayer().get(a).figure.wartosc > playerB.getDeckPlayer().get(a).figure.wartosc) {
                playerA.getDeckPlayer().add(playerA.getDeckPlayer().get(a));
                playerA.getDeckPlayer().add(playerA.getDeckPlayer().get(b));
                playerA.getDeckPlayer().addAll(zdobyczeWojenne);
                playerA.getDeckPlayer().remove(a);
                playerB.getDeckPlayer().remove(b);
                zdobyczeWojenne.clear();
                WarCounter.WAR_COUNTER.setWinOfA(WarCounter.WAR_COUNTER.getWinOfA()+1);
                WarCounter.WAR_COUNTER.setCountOfWar(WarCounter.WAR_COUNTER.getCountOfWar()+1);
                WarCounter.WAR_COUNTER.setTheBiggestWar(WarCounter.WAR_COUNTER.getTempBiggestWar()+1);
                WarCounter.WAR_COUNTER.setTempBiggestWar(0);


            } else {
                playerA.getDeckPlayer().add(playerA.getDeckPlayer().get(a));
                playerB.getDeckPlayer().add(playerB.getDeckPlayer().get(b));
                playerB.getDeckPlayer().addAll(zdobyczeWojenne);
                playerA.getDeckPlayer().remove(a);
                playerB.getDeckPlayer().remove(b);
                zdobyczeWojenne.clear();
                WarCounter.WAR_COUNTER.setWinOfB(WarCounter.WAR_COUNTER.getWinOfB()+1);
                WarCounter.WAR_COUNTER.setCountOfWar(WarCounter.WAR_COUNTER.getCountOfWar()+1);
                WarCounter.WAR_COUNTER.setTheBiggestWar(WarCounter.WAR_COUNTER.getTempBiggestWar()+1);
                WarCounter.WAR_COUNTER.setTempBiggestWar(0);



            }

        }
        warCount++;
    }


    public void carriedOutGame() {

        do {

            if (playerA.getDeckPlayer().size() == 0) {
                System.out.println("Zwyciężył gracz B");
                return;
            } else if (playerB.getDeckPlayer().size() == 0) {
                System.out.println("Zwyciężył gracz A");
                return;
            } else {

                if (playerA.getDeckPlayer().get(0).figure.wartosc > playerB.getDeckPlayer().get(0).figure.wartosc) {
                    playerA.getDeckPlayer().add(playerA.getDeckPlayer().get(0));
                    playerA.getDeckPlayer().add(playerB.getDeckPlayer().get(0));
                    playerA.getDeckPlayer().remove(0);
                    playerB.getDeckPlayer().remove(0);
                } else if (playerA.getDeckPlayer().get(0).figure.wartosc < playerB.getDeckPlayer().get(0).figure.wartosc) {
                    playerB.getDeckPlayer().add(playerA.getDeckPlayer().get(0));
                    playerB.getDeckPlayer().add(playerB.getDeckPlayer().get(0));
                    playerA.getDeckPlayer().remove(0);
                    playerB.getDeckPlayer().remove(0);
                } else {
                    war(0, 0);
                }
            }
            iteration++;
        } while (playerA.getDeckPlayer().size() != 0 || playerB.getDeckPlayer().size() != 0);
    }
}
