package Singleton.zad3_GraWWojne;

public enum Figure {

    AS(14),
    KROL(13),
    DAMA(12),
    WALET(11),
    DZIESIATKA(10),
    DZIEWIATKA(9),
    OSEMKA(8),
    SIODEKA(7),
    SZOSTKA(6),
    PIATKA(5),
    CZWORKA(4),
    TROJKA(3),
    DWOJKA(2);

    int wartosc;
    Figure(int wartosc){this.wartosc = wartosc;}

    public String getName(){
        String nazwa = this.toString();
        nazwa = nazwa.substring(0,1).toUpperCase() + nazwa.toLowerCase().substring(1);
        return String.format(nazwa);
    }
}
