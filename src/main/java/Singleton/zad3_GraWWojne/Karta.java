package Singleton.zad3_GraWWojne;

public class Karta {

    Color color;
    Figure figure;


    public Karta() {
        this.color = color;
        this.figure = figure;
    }

    public Karta(Color color, Figure figure) {
        this.color = color;
        this.figure = figure;
    }


    @Override
    public String toString() {
        return "Karta{" +
                 color.getSymbol() +" " +
                 figure.getName() +
                '}';
    }
}
