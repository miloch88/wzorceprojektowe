package Singleton.zad3_GraWWojne;

import java.util.ArrayList;
import java.util.List;

public class Deck {

    List<Karta> deck = new ArrayList<>();
    Karta karta;
    Figure figure;
    Color color;

    public Deck() {
        List <Karta> deck = new ArrayList<>();
    }

    public void createDeck() {{
        for (Figure f : Figure.values()) {
            for (Color k : Color.values()) {
                deck.add(new Karta(k, f));
            }
        }
    }
    }

    @Override
    public String toString() {
        return "Deck{" +
                "deck=" + deck +
                '}';
    }


}
