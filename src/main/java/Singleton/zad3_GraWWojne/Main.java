package Singleton.zad3_GraWWojne;

public class Main {
    public static void main(String[] args) {


        Player playerA = new Player();
        Player playerB = new Player();
        Deck deck = new Deck();
        deck.createDeck();
        Game game = new Game(playerA, playerB, deck);
        game.dealCards();

//        System.out.println(playerA);
//        System.out.println(playerB);

        game.carriedOutGame();

        System.out.println("Ilość wygranych wojen prze gracza A: " + WarCounter.WAR_COUNTER.getWinOfA());
        System.out.println("Ilość wygranych wojen prze gracza B: " + WarCounter.WAR_COUNTER.getWinOfB());
        System.out.println("Ilość rozegranych wojen: " + WarCounter.WAR_COUNTER.getCountOfWar());
        System.out.println("Wielkość największej wojny: " + WarCounter.WAR_COUNTER.getTheBiggestWar());

//        System.out.println(playerA.getDeckPlayer().size());
//        System.out.println(playerB.getDeckPlayer().size());
//        System.out.println("Liczba gier: " + game.iteration);
//
//        System.out.println(playerA);
//        System.out.println(playerB);


}
}
