package ObserverObservable.przyklad_new;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;

public class Portal {

//  Property jest zdefiniowane jako String a powinna być typem generycznym
//    private StringProperty newsProperty = new SimpleStringProperty();
//
//    public void addObserver(ChangeListener<String> stringChangeListener){
//        newsProperty.addListener(stringChangeListener);
//    }
//
//    public void wyslijWiadomosc(String wiadomosc){
//        newsProperty.setValue(wiadomosc);
//    }

    private ObjectProperty<Wiadomosc> wiadomoscObjectProperty = new SimpleObjectProperty<>();

    public void addObserver(ChangeListener<Wiadomosc> wiadomoscChangeListener) {
        wiadomoscObjectProperty.addListener(wiadomoscChangeListener);
    }

    public void wyslijWiadomosc(Wiadomosc wiadomosc){
        wiadomoscObjectProperty.setValue(wiadomosc);
    }
}
