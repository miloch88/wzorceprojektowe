package ObserverObservable.przyklad_new;

public class Main {
    public static void main(String[] args) {

     Portal portal = new Portal();

     portal.addObserver(new Uzytkownik("Kasia"));
     portal.addObserver(new Uzytkownik("Basia"));


     portal.wyslijWiadomosc(new Wiadomosc());
    }
}
