package ObserverObservable.przyklad.library;

public class Main {
    public static void main(String[] args) {

        Portal portal = new Portal();

        portal.addObserver(new Uzytkownik("Marian"));
        portal.addObserver(new Uzytkownik("Szczepan"));
        portal.addObserver(new Uzytkownik("Dorian"));

        portal.powiadom("Bułki podrożałyn");
    }
}
