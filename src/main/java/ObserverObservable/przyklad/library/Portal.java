package ObserverObservable.przyklad.library;

import java.util.Observable;

public class Portal extends Observable {

    public void powiadom(String wiadomosc){
        setChanged(); // ustawiamy flagę "zmieniłem się" (jako obiekt)
        notifyObservers(wiadomosc); // rozsyłamy wiadomość
    }
}
