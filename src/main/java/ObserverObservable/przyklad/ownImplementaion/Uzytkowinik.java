package ObserverObservable.przyklad.ownImplementaion;

public class Uzytkowinik implements Powiadamialny {
    private String imie;

    public Uzytkowinik(String imie){
        this.imie = imie;
    }

    @Override
    public void powiadom(Object oferta) {
        if(oferta instanceof Oferta){
            Oferta jakasOferta = (Oferta) oferta;
            System.out.println("Otrzymałem(" + imie + ") oferte: "
                    + jakasOferta.getWiadomosc() + " za: "+ jakasOferta.getCena());

        }else{
            System.out.println((imie + " powiadomoiny o: " + oferta ));
        }
    }
}
