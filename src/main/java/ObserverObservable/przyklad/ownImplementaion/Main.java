package ObserverObservable.przyklad.ownImplementaion;

public class Main {
    public static void main(String[] args) {

        Portal allegro = new Portal();

        allegro.dodajSubskrybenta(new Uzytkowinik("Marian"));
        allegro.dodajSubskrybenta(new Uzytkowinik("Szczepan"));
        allegro.dodajSubskrybenta(new Uzytkowinik("Micahał"));

        allegro.wyslijOferte("Nowy laptop za pół ceny");

        allegro.wyslijOferte(new Oferta("Bułki", 2.30));


    }
}
