package ObserverObservable.przyklad.ownImplementaion;

public class Oferta {
    private String wiadomosc;
    private double cena;

    public Oferta(String wiadomosc, double cena) {
        this.wiadomosc = wiadomosc;
        this.cena = cena;
    }

    public String getWiadomosc() {
        return wiadomosc;
    }

    public double getCena() {
        return cena;
    }
}
