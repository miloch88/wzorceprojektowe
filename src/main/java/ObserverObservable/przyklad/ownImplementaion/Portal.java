package ObserverObservable.przyklad.ownImplementaion;

import java.util.ArrayList;
import java.util.List;

public class Portal {
    //observale

    private List<Powiadamialny> uzytkonikList = new ArrayList<>();

    public void dodajSubskrybenta(Powiadamialny uzytkoniwk){
        uzytkonikList.add(uzytkoniwk);
    }

    public void wyslijOferte(Object oferta){
        for(Powiadamialny user : uzytkonikList){
            user.powiadom(oferta);
        }
    }
}
