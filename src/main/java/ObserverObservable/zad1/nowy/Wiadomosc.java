package ObserverObservable.zad1.nowy;

public class Wiadomosc {

    private String tresc;
    private int wagaWiadomosci;

    public Wiadomosc(String tresc, int wagaWiadomosci) {
        this.tresc = tresc;
        this.wagaWiadomosci = wagaWiadomosci;
    }

    public String getTresc() {
        return tresc;
    }

    public void setTresc(String tresc) {
        this.tresc = tresc;
    }

    public int getWagaWiadomosci() {
        return wagaWiadomosci;
    }

    public void setWagaWiadomosci(int wagaWiadomosci) {
        this.wagaWiadomosci = wagaWiadomosci;
    }

    @Override
    public String toString() {
        return tresc;
    }
}
