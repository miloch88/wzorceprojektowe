package ObserverObservable.zad1.nowy;


import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;

public class NewStation {

    private ObjectProperty<Wiadomosc> wiadomoscObjectProperty = new SimpleObjectProperty<>();

    public void addObserver(ChangeListener<Wiadomosc> wiadomoscChangeListener){
        wiadomoscObjectProperty.addListener(wiadomoscChangeListener);
    }

    public void wyslijWiadomosc(Wiadomosc wiadomosc){
        wiadomoscObjectProperty.setValue(wiadomosc);
    }
}
