package ObserverObservable.zad1.nowy;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

public class Watcher implements ChangeListener<Wiadomosc> {

    private String imie;
    private int poziomPaniki;

    public Watcher(String imie, int poziomPaniki) {
        this.imie = imie;
        this.poziomPaniki = poziomPaniki;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public int getPoziomPaniki() {
        return poziomPaniki;
    }

    public void setPoziomPaniki(int poziomPaniki) {
        this.poziomPaniki = poziomPaniki;
    }


    /**
     * This method needs to be provided by an implementation of
     * {@code ChangeListener}. It is called if the value of an
     * {@link ObservableValue} changes.
     * <p>
     * In general is is considered bad practice to modify the observed value in
     * this method.
     *
     * @param observable The {@code ObservableValue} which value changed
     * @param oldValue   The old value
     * @param newValue
     */
    @Override
    public void changed(ObservableValue<? extends Wiadomosc> observable, Wiadomosc oldValue, Wiadomosc newValue) {
        if(newValue.getWagaWiadomosci() < getPoziomPaniki()){
            System.out.println("(" + imie + ") Jestem powiadomiona o wydarzeniu: " + newValue.toString());
        }else{
            System.out.println(imie + " wpadła w panikę");
        }

    }
}
