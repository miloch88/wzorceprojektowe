package ObserverObservable.zad1.nowy;

public class Main {
    public static void main(String[] args) {

        NewStation newStation = new NewStation();

        newStation.addObserver(new Watcher("Zosia SamaNieWieJaka", 5));
        newStation.addObserver(new Watcher("Kasia Lwie Swerce", 9));
        newStation.addObserver(new Watcher("Basia Bojaźliwa", 1));
        newStation.addObserver(new Watcher("Basia Bojaźliwa", 10));


        newStation.wyslijWiadomosc(new Wiadomosc("Kosmici atakują", 4));

        System.out.println("Ale będzie dobrze");

    }
}
