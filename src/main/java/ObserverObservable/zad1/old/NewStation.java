package ObserverObservable.zad1.old;

import java.util.Observable;

public class NewStation extends Observable {

    public void powiadom(Wiadomosc wiadomosc){
        setChanged();
        notifyObservers(wiadomosc);
    }

}
