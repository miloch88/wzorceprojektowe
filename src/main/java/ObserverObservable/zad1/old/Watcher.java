package ObserverObservable.zad1.old;

import java.util.Observable;
import java.util.Observer;

public class Watcher implements Observer {

    private String imie;
    private int poziomPaniki;

    public Watcher(String imie, int poziomPaniki) {
        this.imie = imie;
        this.poziomPaniki = poziomPaniki;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public int getPoziomPaniki() {
        return poziomPaniki;
    }

    public void setPoziomPaniki(int poziomPaniki) {
        this.poziomPaniki = poziomPaniki;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Wiadomosc) {
            Wiadomosc wiadomoscObslugiwana = (Wiadomosc) arg;
            if (wiadomoscObslugiwana.getWagaWiadomosci() < getPoziomPaniki()) {
                System.out.println(imie + " została powiadomiony o: " + wiadomoscObslugiwana.getTresc());
            } else {
                System.out.println(imie + " wpadła w panikę");
            }
        }
    }
}
