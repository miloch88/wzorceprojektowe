package ObserverObservable.zad1.old;

public class Main {
    public static void main(String[] args) {

        NewStation newStation = new NewStation();

        newStation.addObserver(new Watcher("Zosia SamaNieWieJaka", 5));
        newStation.addObserver(new Watcher("Kasia Lwie Swerce", 9));
        newStation.addObserver(new Watcher("Basia Bojaźliwa",1 ));


        newStation.powiadom(new Wiadomosc("Fala upałów nad Polską", 4));
        System.out.println();
        newStation.powiadom(new Wiadomosc("Inwazja kosmitów", 8));

        System.out.println();
//      newStation.powiadom("Meteorycie wielkości Polski");
        Wiadomosc wiadomosc = new Wiadomosc("Meteoryt wielkości Teksasu zbliża się do Poslki", 10);
        newStation.powiadom(wiadomosc);

    }
}
